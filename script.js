window.onload = function () {
  // Month Day, Year Hour:Minute:Second, id-of-element-container
  countUpFromTime("Oct 1, 2022 12:00:00", "countup1"); // ****** Change this line!
};
function countUpFromTime(countFrom, id) {
  countFrom = new Date(countFrom).getTime();
  var now = new Date(),
    countFrom = new Date(countFrom), 
    timeDifference = now - countFrom;

  var secondsInADay = 60 * 60 * 1000 * 24,
    days = Math.floor((timeDifference / secondsInADay) * 1);
  years = Math.floor(days / 365);
  if (years > 1) {
      var idEl = document.getElementById(id);
    idEl.getElementsByClassName("years").style.visibility = "hidden"
    days = days - years * 365;
  }
  months = Math.floor(timeDifference / secondsInADay / 30);
  weeks = Math.floor(days - (months * 30));

  var idEl = document.getElementById(id);
  idEl.getElementsByClassName("years")[0].innerHTML = years;
  idEl.getElementsByClassName("days")[0].innerHTML = weeks;
  idEl.getElementsByClassName("months")[0].innerHTML = months;

  clearTimeout(countUpFromTime.interval);
  countUpFromTime.interval = setTimeout(function () {
    countUpFromTime(countFrom, id);
  }, 1000);
}

function bt1() {
  document.getElementById("love-chat").style.visibility = "hidden";
      document.getElementById("love-gallery").style.visibility = "hidden";

}

function bt2() {
  document.getElementById("love-chat").style.visibility = "hidden";
      document.getElementById("love-gallery").style.visibility = "visible";
  
}

function bt3() {
  document.getElementById("love-chat").style.visibility = "visible";
      document.getElementById("love-gallery").style.visibility = "hidden";

}
